
import os
import unittest
from ellipsyswrapper.testsuite import EllipSysTestCase

# check that the user has set the ELLIPSYS2D_PATH
if os.getenv('ELLIPSYS3D_PATH') == None:
    raise RuntimeError('ELLIPSYS3D_PATH environment variable not set!')


class FFA_W3_241_pseudo3d_turb_complex(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver': 'ellipsys3d',
                         'directory': 'ellipsys3d_aero_testcases/FFA-W3-241-pseudo3d/re10e6turb_cs',
                         'inputfile': 'input.dat',
                         'inputs':[{'mstep': [100, 100, 100]},
                                   {'uinlet': 1.0+1.e-40j}, {'vinlet': .0},
                                   {'ufarfield': 1.0+1.e-40}, {'vfarfield': .0},
                                   {'project': 'grid'}],
                         'test_restart': False,
                         'flowfield': 'c_flowfieldMPI',
                         'debug': True,
                         'nstep_ave': 20,
                         'variables': ['fx_ave', 'fy_ave'],
                         'nprocs': 4}


class DTU_10MW_RWT_w08turb_complex(unittest.TestCase, EllipSysTestCase):

    def setUp(self):

        self.casedict = {'solver': 'ellipsys3d',
                         'directory': 'ellipsys3d_aero_testcases/DTU_10MW_RWT/w08turb',
                         'inputfile':'input.dat',
                         'inputs':[{'mstep':[50, 100, 100]},
                                   {'project':'grid'},
                                   {'winlet': 8.+1.e-40j},
                                   {'wfarfield': 8.+1e-40j}],
                         'variables':['fz_ave', 'mz_ave'],
                         'flowfield': 'c_flowfieldMPI',
                         'cs_mode': 'realpart',
                         'test_restart': False,
                         'debug': True,
                         'nstep_ave': 20,
                         'nprocs': 4}



if __name__ == '__main__':

    unittest.main()

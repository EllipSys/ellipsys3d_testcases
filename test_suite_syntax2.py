
import os
import unittest
from ellipsyswrapper.testsuite import EllipSysTestCase

# check that the user has set the ELLIPSYS3D_PATH
if os.getenv('ELLIPSYS3D_PATH') == None:
    raise RuntimeError('ELLIPSYS3D_PATH environment variable not set!')


class SyntaxTest_UnusedVariables(unittest.TestCase):


    def setUp(self):

        groups = self.groups = {}
        
        # if there are a few cases where we allow exceptions then explain them here:
        allowed_incidents = 0

        groups['NavierStokes'] = {}
        groups['NavierStokes']['allowed_serial'] = 0
        groups['NavierStokes']['allowed_mpi'] = 0
        groups['NavierStokes']['ignored_files'] = []

        groups['Platform'] = {}
        groups['Platform']['allowed_serial'] = 0
        groups['Platform']['allowed_mpi'] = 0
        groups['Platform']['ignored_files'] = []

        groups['ko2turb'] = {}
        groups['ko2turb']['allowed_serial'] = 4  # ko2_interface.f: 4 allowed nsqr-formats
        groups['ko2turb']['allowed_mpi'] = 4  # ko2_interface.f: 4 allowed nsqr-formats
        groups['ko2turb']['ignored_files'] = []

        groups['Transition'] = {}
        groups['Transition']['allowed_serial'] = 0
        groups['Transition']['allowed_mpi'] = 0
        groups['Transition']['ignored_files'] = []

        groups['ACLine'] = {}
        groups['ACLine']['allowed_serial'] = 0
        groups['ACLine']['allowed_mpi'] = 0
        groups['ACLine']['ignored_files'] = []

        groups['overlap'] = {}
        groups['overlap']['allowed_serial'] = 0
        groups['overlap']['allowed_mpi'] = 0
        groups['overlap']['ignored_files'] = ['boundata.f', 'holecut.f', 'boundaryray.f']

        groups['InverseMap'] = {}
        groups['InverseMap']['allowed_serial'] = 0
        groups['InverseMap']['allowed_mpi'] = 0
        groups['InverseMap']['ignored_files'] = []

        groups['ibm'] = {}
        groups['ibm']['allowed_serial'] = 68
        groups['ibm']['allowed_mpi'] = 68
        groups['ibm']['ignored_files'] = []

        groups['Forest'] = {}
        groups['Forest']['allowed_serial'] = 0
        groups['Forest']['allowed_mpi'] = 0
        groups['Forest']['ignored_files'] = []

        groups['ActuatorShape1'] = {}
        groups['ActuatorShape1']['allowed_serial'] = 0
        groups['ActuatorShape1']['allowed_mpi'] = 0
        groups['ActuatorShape1']['ignored_files'] = []

    def test_syntax(self):
        """ 
        Test number of debug warnings
 
        This can ideally be checked at compile time by adding the fol-
        lowing line to the flowfield.mkf:
        FFLAGS += -fcheck=all -Wunused -Werror=unused-variable
        However, it is tiresome to use this debug option as long as only some of
        the source code folders have been checked. Therefore, we currently only
        have:
        FFLAGS += -fcheck=all -Wunused
        and the we use Python below to make checks on the compilation output
        files (compileSERIAL.out and compileMPI.out).
        """

        # then make checks regarding syntax
        # libs
        import sys
        import numpy as np

        ################### Test02: checking unused variables in cleaned folders
        # dirs and folders
        e3d_dir = os.getenv('ELLIPSYS3D_PATH')
        for fldr, d in self.groups.items():

            # list 1: check for the compileSERIAL.out file
            stream1 = os.popen('grep -ri \'sourcesmpi/{:s}\' {:s}/compileSERIAL.out'.format(fldr,e3d_dir))
            lst1 = stream1.readlines()
            for ignore in self.groups[fldr]['ignored_files']:
                lst1 = list(filter(lambda x: ignore not in x, lst1))

            # list 2: check for the compileMPI.out file
            stream2 = os.popen('grep -ri \'sourcesmpi/{:s}\' {:s}/compileMPI.out'.format(fldr,e3d_dir))
            lst2 = stream2.readlines()
            lst2t = []
            def condition(string, lst):
                return np.all(filter(lambda ignore, string: ignore1 not in string, filterlist))
            filtnotin = lambda x, lst: ignore not in x
            for ignore in self.groups[fldr]['ignored_files']:
                lst2 = list(filter(lambda x: ignore not in x, lst2))
                         

            stream1.close()
            stream2.close()

            self.groups[fldr]['warnings_serial'] = lst1 
            self.groups[fldr]['warnings_mpi'] = lst2 

            # now, we check that developers did not commit sloppy Fortran code syntax
            for comp, lst in zip(('serial', 'mpi'), (lst1, lst2)):
                with self.subTest(variable=fldr):
                    if len(lst) > self.groups[fldr]['allowed_' + comp]:
                        print('')
                        print('========================================================================')
                        print('Folder %s has too many warnings compiled with %s' % (fldr, comp), flush=True)
                        print('========================================================================')
                        for fd in lst:
                            print(fd.strip('\n'))
                        raise RuntimeError('Syntax warnings test failed for folder %s, actual=%i, allowed=%i' %
                                           (fldr, len(lst), self.groups[fldr]['allowed_'+comp]))
    

if __name__ == '__main__':

    unittest.main()

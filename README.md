# EllipSys3D Test Suite Runner

This repository contains the script for running the test cases for EllipSys3D.
This repository does not contain the actual test cases, since these are
contained in individual repositories according to the type of test case.


## Prerequisites

To use the automated test case runner, you need to have Python 2.7.x installed.
An easy way to installing this is to use Miniconda:

    # choose a location to install miniconda
    $ export CONDA_ENV_PATH=/path/to/miniconda
    $ export PATH=$CONDA_ENV_PATH/bin:$PATH

    $ wget --quiet \
    https://repo.continuum.io/miniconda/Miniconda-latest-Linux-x86_64.sh && \
    bash Miniconda-latest-Linux-x86_64.sh -b -p $CONDA_ENV_PATH && \
    rm Miniconda-latest-Linux-x86_64.sh && \
    chmod -R a+rx $CONDA_ENV_PATH

    $ conda update --quiet --yes conda \
      && conda create -y -n py27 python=2.7 \
      && /bin/bash -c "source activate py27 \
      && conda install pip numpy scipy nose hdf5"


## Installation

To run the test cases you need to install the `ellipsyswrapper` module, which
is a simple Python wrapper for EllipSys2D and EllipSys3D, that also contains
a script for executing test cases.

First step is to clone and install the `ellipsyswrapper` git repository

    $ source activate py27
    $ pip install git+https://gitlab.windenergy.dtu.dk/EllipSys/ellipsyswrapper.git@master

You also need to specify where the Python wrapper can find the executables for EllipSys.
This is done by setting an environment variable:

    $ export ELLIPSYS3D_PATH=/path/to/ellipsys3d/


### Downloading testcases

The following test suites are available:

* aero_testcases: Test cases related to 3D airfoil flows and rotor flows, see https://gitlab.windenergy.dtu.dk/EllipSys/ellipsys3d_aero_testcases
* acshape_testcases: Test cases related to simulations of geometries represented by Actuator Shapes, see https://gitlab.windenergy.dtu.dk/EllipSys/ellipsys3d_acshape_testcases
* abl_testcases: Test cases related to simulations of the atmospheric boundary layer, see https://gitlab.windenergy.dtu.dk/EllipSys/ellipsys3d_abl_testcases
* forest_testcases: Test cases related to simulations of flow over forested areas, see https://gitlab.windenergy.dtu.dk/EllipSys/ellipsys3d_forest_testcases
* al_testcases: Test cases related to the Actuator Line model, see https://gitlab.windenergy.dtu.dk/EllipSys/ellipsys3d_al_testcases
* ibm_testcases: Test cases related to the immersed boundary method, see https://gitlab.windenergy.dtu.dk/EllipSys/ellipsys3d_ibm_testcases

Running the installation script `setup.py` will by default clone all test case
repositories:

    $ python setup.py install
    
If you only wish to install a sub-set of the test cases, you can do so by adding the argument `--modules` to the install command:

    $ python setup.py install --modules aero_testcases


## Running the testsuite

With the above steps completed you can now run the test suite, simply by invoking the command:

    $ python test_suite.py
    
which should finalize with an `OK`.
    

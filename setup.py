
import subprocess
import os
import sys
from setuptools import setup
from collections import OrderedDict

"""
Script for installation of testcases

Arguments
---------

modules: list
    list of modules to install, defaults to all five: abl_testcases, acshape_testcases,
    aero_testcases, al_testcases, forest_testcases
branches: list
    branches of the five test case repos, must be specified either as one branch for all
    or five branch names in the order abl_testcases, acshape_testcases,
    aero_testcases, al_testcases, forest_testcases.
"""



def install_modules(modules, branches):

    for mod, branch in zip(modules, branches):
        print('cloning repo %s branch %s' % (mod, branch), flush=True)
        if os.environ.get('CI_JOB_ID', None):
            parts = mod.split('//')
            url = parts[0] +'//' + 'ellipsys_ci_bot:$BOT_ACCESS_TOKEN@' + parts[1]
        else:
            url = mod
        cmd = 'git clone %s' % (url)
        print('CMD', cmd, flush=True)
        out = subprocess.check_output(cmd, shell=True)
        print(out, flush=True)
        name = mod.split('/')[-1].split('.')[0]
        exists = subprocess.check_output('git -C %s branch -a --list %s' % (name, 'origin/'+branch), shell=True)
        if exists is not None:
            os.system('git -C %s checkout %s' % (name, branch))
        else:
            print('branch %s does NOT exist, using master')


# dependencies can be installed automatically
modules = {'abl_testcases': 'https://gitlab.windenergy.dtu.dk/EllipSys/ellipsys3d_abl_testcases.git',
           'acshape_testcases': 'https://gitlab.windenergy.dtu.dk/EllipSys/ellipsys3d_acshape_testcases.git',
           'aero_testcases': 'https://gitlab.windenergy.dtu.dk/EllipSys/ellipsys3d_aero_testcases.git',
           'forest_testcases': 'https://gitlab.windenergy.dtu.dk/EllipSys/ellipsys3d_forest_testcases.git',
           'al_testcases': 'https://gitlab.windenergy.dtu.dk/EllipSys/ellipsys3d_al_testcases.git',
           'lidar_testcases': 'https://gitlab.windenergy.dtu.dk/EllipSys/ellipsys3d_lidar_testcases.git',
           'wtu_testcases': 'https://gitlab.windenergy.dtu.dk/EllipSys/ellipsys3d_wtu_testcases.git',
           'ibm_testcases': 'https://gitlab.windenergy.dtu.dk/EllipSys/ellipsys3d_ibm_testcases.git'}

# sorted alphabetically
modules = OrderedDict(sorted(modules.items(), key=lambda t: t[0]))

args = sys.argv

mods = []
if '--modules' in args:
    ix = args.index('--modules')
    if '--branches' in args[ix+1:]:
        ix1 = args.index('--branches')
    else:
        ix1 = None
    for name in args[ix+1:ix1]:
        try:
            mods.append(modules[name])
        except:
            print('Error in test cases name: %s' % name)
            print('The following test case repos are available:')
            print(' \n'.join(map(str, modules.keys())))
else:
    mods = modules.values()

if '--branches' in args:
    ix = args.index('--branches')
    if '--modules' in args[ix+1:]:
        ix1 = args.index('--modules')
    else:
        ix1 = None
    branches = []
    for name in args[ix+1:ix1]:
        branches.append(name)
    if len(branches) == 1:
        branches = branches * len(mods)
else:
    branches = ['master'] * len(mods)

install_modules(mods, branches=branches)


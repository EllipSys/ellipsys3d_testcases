
import os
import unittest
from collections import Counter
from ellipsyswrapper.testsuite import EllipSysTestCase

# check that the user has set the ELLIPSYS3D_PATH
if os.getenv('ELLIPSYS3D_PATH') == None:
    raise RuntimeError('ELLIPSYS3D_PATH environment variable not set!')


class SyntaxTest_UseOnly(unittest.TestCase):

    def setUp(self):

        self.groups = {}

        # list of EllipSys models checked for syntax and their allowed use statements
        # without use, only:

        # NavierStokes
        allowed_incidents = 1 # 'use iso_c_binding' in ns_ext_interface.f
        allowed_incidents += 1 # 'use BCCOmom_interface' in momentum.f
        self.groups['NavierStokes'] = {}
        self.groups['NavierStokes']['allowed'] = allowed_incidents

        # Platform
        allowed_incidents = 26 # lib3dcv.f not cleaned
        allowed_incidents += 1 # 'use MPI' in MPIheader.f
        allowed_incidents += 1 # 'use MPI' in MPIparallel.f
        allowed_incidents += 68 # 'use MPIheader' in MPIparallel.f
        allowed_incidents += 1 # 'use iso_c_binding' in platform_ext_interface.f
        allowed_incidents += 91 # complexify.f
        self.groups['Platform'] = {}
        self.groups['Platform']['allowed'] = allowed_incidents

        # ko2turb
        allowed_incidents = 0
        self.groups['ko2turb'] = {}
        self.groups['ko2turb']['allowed'] = allowed_incidents

        # Transition
        allowed_incidents = 9 # 3 cases in each of the 3 OLD*.f files
        self.groups['Transition'] = {}
        self.groups['Transition']['allowed'] = allowed_incidents

        # ACLine
        allowed_incidents = 0
        self.groups['ACLine'] = {}
        self.groups['ACLine']['allowed'] = allowed_incidents

        # overlap
        allowed_incidents = 12  # holecut.f
        allowed_incidents += 25  # holecut.f
        allowed_incidents += 1  # map.f 
        self.groups['overlap'] = {}
        self.groups['overlap']['allowed'] = allowed_incidents

        # InverseMap
        allowed_incidents = 1  # inversemap_ext_interface.f:'use iso_c_binding'
        self.groups['InverseMap'] = {}
        self.groups['InverseMap']['allowed'] = allowed_incidents

        # IBM
        allowed_incidents = 0  # inversemap_ext_interface.f:'use iso_c_binding'
        self.groups['ibm'] = {}
        self.groups['ibm']['allowed'] = allowed_incidents

        # Forest
        allowed_incidents = 1  # forest_ext_interface.f:'use iso_c_binding'
        self.groups['Forest'] = {}
        self.groups['Forest']['allowed'] = allowed_incidents

        # ActuatorShape1
        allowed_incidents = 1  # acshape1_ext_interface.f:'use iso_c_binding'
        self.groups['ActuatorShape1'] = {}
        self.groups['ActuatorShape1']['allowed'] = allowed_incidents


    def test_syntax(self):
        """ 
        test use, only syntax

        All tests are at the moment based
        on Python since we this way easily can add folders as they are cleaned.
        However, once the entire source code has been clean for syntax there are
        more elegant ways to make some of the checks. To give one example we
        point to Test02 which checks that no variales are declared but left un-
        used. This can ideally be checked at compile time by adding the fol-
        lowing line to the flowfield.mkf:
        FFLAGS += -fcheck=all -Wunused -Werror=unused-variable
        However, it is tiresome to use this debug option as long as only some of
        the source code folders have been checked. Therefore, we currently only
        have:
        FFLAGS += -fcheck=all -Wunused
        and the we use Python below to make checks on the compilation output
        files (compileSERIAL.out and compileMPI.out).
        """

        # then make checks regarding syntax
        # libs
        import sys
        import numpy as np


        # dirs and folders
        e3d_dir = os.getenv('ELLIPSYS3D_PATH')
        for fldr, d in self.groups.items(): 
            e3d_dir_fldr = e3d_dir.replace('/Executables','/SourcesMPI/{:s}/'.format(fldr))
            print('Checking folder', e3d_dir_fldr, flush=True)

            # list 1: check for the \'      use \'-pattern
            stream1 = os.popen('grep -ri \'      use \' `find {:s} -type f -name \'*f\' \( -iname "*.*" ! -iname  "c_*" \)`'.format(e3d_dir_fldr))
            lst_all = stream1.readlines()

            # list 2: check that all those cases have a \' only:\'-pattern
            stream2 = os.popen('grep -ri \'      use \' `find {:s} -type f -name \'*f\' \( -iname "*.*" ! -iname  "c_*" \)`  | grep \' only:\''.format(e3d_dir_fldr))
            lst_only = stream2.readlines()
            res = list((Counter(lst_all) - Counter(lst_only)).elements())

            self.groups[fldr]['missing'] = res

            stream1.close()
            stream2.close()

            # now, we check that developers did not commit sloppy Fortran code syntax
            with self.subTest(variable=fldr):
                if len(res) > self.groups[fldr]['allowed']:
                    print('')
                    print('========================================================================')
                    print('Folder %s has too many occurences of use statements without ", only"' % fldr, flush=True)
                    print('========================================================================')
                    for fd in res:
                        print(fd.strip('\n'))
                    raise RuntimeError('Syntax use, only test failed for folder %s, actual=%i, allowed=%i' % 
                                       (fldr, len(res), self.groups[fldr]['allowed']))



if __name__ == '__main__':

    unittest.main()
